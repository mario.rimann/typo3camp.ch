---
title: "Sponsoren"
weight: 80
aliases: sponsoren.html
date: 2023-12-03T12:00:00+01:00
draft: false
menu: main
---

{{< svg-sprite-loader sponsors >}}

## Sponsor werden

Das TYPO3camp Schweiz finanziert sich vorwiegend durch das Sponsoring. Für die Teilnehmer fällt lediglich eine kleine Gebühr in Form eines Event-Tickets an.

Als Sponsor unterstützt Ihr nicht nur die Open-Source-Idee sondern Ihr profitiert als Sponsor vor, während und nach der Veranstaltung von eurem Beitrag. Alle Sponsoren werden medienwirksam in Szene gesetzt. Nutzt die Chance, gezielt die Kernzielgruppe Entwickler, Integratoren und Entscheidungsträger der TYPO3-Szene sowie angrenzender Technologiebereiche anzusprechen.

Wir freuen uns, auch euch/dich als Sponsor gewinnen zu können. Weitere Informationen haben wir in der [Projektbeschreibung](projektbeschreibung_typo3camp_2024.pdf) zusammengestellt. Bei Fragen wende dich bitte an [Andri](mailto:andri.steiner@typo3.org).

## Gold Sponsoren

{{< paragraph >}}
{{< sponsor-tile opsone www.opsone.ch "Gold Sponsor: Ops One AG" >}}
{{< sponsor-tile pluswerk www.pluswerk.ag "Gold Sponsor: +Pluswerk AG" >}}
{{< /paragraph >}}

## Silber Sponsoren
{{< paragraph >}}
{{< sponsor-tile hausformat www.hausformat.com "Silber Sponsor: .hausformat GmbH" >}}
{{< sponsor-tile internezzo www.internezzo.ch "Silber Sponsor: internezzo ag" >}}
{{< sponsor-tile visol www.visol.ch "Silber Sponsor:  visol digitale Dienstleistungen AG" >}}
{{< sponsor-tile lst www.lst.team "Silber Sponsor:  LST AG" >}}
{{< /paragraph >}}

## Bronze Sponsoren
{{< paragraph >}}
{{< sponsor-tile hostpoint www.hostpoint.ch "Bronze Sponsor: Hostpoint AG" >}}
{{< /paragraph >}}

## Organisatoren
{{< sponsor-tile opsone www.opsone.ch "Organisator: Ops One AG" >}}
{{< sponsor-tile intersim www.intersim.ch "Organisator: Intersim AG" >}}
{{< sponsor-tile rtp www.rtp.ch "Organisator: RTP GmbH" >}}