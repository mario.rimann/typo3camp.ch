---
title: "Programm"
weight: 50
draft: false
menu: main
---

## Donnerstag, 18.04.2024 (ab 19:00)

Los geht's am Donnerstagabend ab 19:00 im [El Lokal](/location#warmup) in der nähe des Hauptbahnhofes.
Am Warm-Up hast du die Gelegenheit, Teilnehmende schon vorab kennenzulernen
und/oder mit alten Bekannten ein Bierchen zu trinken. Die eine oder andere
Runde wird - wie gewohnt - auf uns gehen :)

Wir freuen uns auf altbekannte und neue Gesichter. Komm vorbei - erst recht, wenn
Du alleine unterwegs bist. Bisher wurde noch niemand gebissen.

## Freitag, 19.04.2024

| Zeit          | Beschreibung                                           |
|---------------|--------------------------------------------------------|
| ab 09.00      | Einlass, Registrierung, Kaffee und Gipfeli             |
| 10:00 - 11:30 | Begrüssung und Sessionplanung                          |
| 11:30 - 12:30 | Sessions                                               |
| 12:30 - 13:30 | Mittagspause                                           |
| 13:30 - 16:30 | Sessions                                               |
| 16:30 - 16:45 | Tagesabschluss / Infos Social Event                    |
| ab 17:30      | [Social Event](/location#social-event-mönchhof-am-see) |


## Samstag, 20.04.2024

| Zeit          | Beschreibung              |
|---------------|---------------------------|
| ab 09:00      | Kaffee und Gipfeli        |
| 10:00 - 10:30 | Sessionplanung            |
| 10:30 - 12:30 | Sessions                  |
| 12:30 - 13:30 | Mittagspause              |
| 13:30 - 15:30 | Sessions / Zertifizierung (buchen im [TYPO3 Shop](https://shop.typo3.com/TYPO3-Certification-TYPO3-Camp-Swiss/TY10146)) |
| 15:30         | Offizielle Verabschiedung |
