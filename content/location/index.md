---
title: "Location"
weight: 60
date: 2023-12-03T12:00:00+01:00
draft: false
menu: main
toc: true
aliases:
- /unterkunft
---

Das TYPO3camp Schweiz geht in die fünfte Runde. 2024 verschlägt es uns nach Zürich! 
Wir freuen uns, zwei spannende Tage in den Räumen der Jugendherberge mit euch zu gestalten und durchzuführen.

![Jugendherberge Zürich - Innenhof](jugi-innenhof.jpg)

## Camp Location

### Anreise
Jugendherberge Zürich \
Mutschellenstrasse 114 \
8038 Zürich

Die Jugi auf [youthhostel.ch](https://www.youthhostel.ch/de/hostels/zuerich) \
Die Jugi auf [Google Maps](https://maps.app.goo.gl/ZVWmQzaiDjq4zz8BA)


#### Öffentliche Verkehrsmittel
Die Jugendherberge ist nur 2 S-Bahn Stationen und ein paar Minuten zu Fuss vom Zürich HB entfernt (via "Zürich Brunau").
Die schnellste Verbindung ab eurem Standort (auch international) findet ihr im [SBB Fahrplan](https://www.sbb.ch/de/kaufen/pages/fahrplan/fahrplan.xhtml?nach=Z%C3%BCrich,%20Jugendherberge&datum=18.04.2024&zeit=17:00&an=true).

#### Auto
Grundsätzlich empfehlen wir, mit den öffentliche Verkehrsmitteln anzureisen. Wer doch gerne motorisiert anreisen möchte,
verlässt die Autobahn A3 am besten bei der Ausfahrt 33 "Wollishofen". Von da sind es noch wenige Minuten bis zur
Jugi. Achtung: Genügend Zeit einberechnen. Der Verkehr rund um Zürich ist oft eher stockend unterwegs.

### Unterkünfte

#### Jugendherberge :)
* Vom Einzelzimmer mit Dusche/Toilette bis zum Mehrbettzimmer mit Dusche/WC auf dem Flur
* 30 Sekunden bis zum Hauptraum des Camps ;)

https://www.youthhostel.ch/de/hostels/zuerich


#### Das Nächstgelegene: Residence Zürich

* Einzel- & Doppelzimmer
* 750m/10min zur Jugendherberge

https://www.residence-mutschellen.com/

## Warmup

Am Warmup hast du die Gelegenheit, Teilnehmende schon vorab kennenzulernen und/oder mit alten Bekannten ein Bierchen zu trinken. Wir treffen uns am Donnerstagabend ab 19:00.

El Lokal  
Gessner-Allee 11  
8001 Zürich

Weitere Infos auf [ellokal.ch](http://www.ellokal.ch/)
oder [Google Maps](https://maps.app.goo.gl/AqZibY7p4nk96gYc8).


## Social Event (Mönchhof am See)
![Möchhof am See](moenchhof_am_see.jpg)
Es zieht uns auch dieses Jahr ans Wasser. Und zwar treffen wir uns
Freitag Abend (nach dem ersten Camptag) beim "[Mönchhof am See](//www.moenchhof-am-see.ch)" in
der Nachbargemeinde. Dieser ist in ca. 20 Minuten mit dem ÖV erreichbar.
Oder mit einem kleinen halbstündigen Spaziergang. Wir dürfen dort einen
schönen Grillabend am Ufer des Zürichsees in entspannter Atmosphäre
verbringen. Mit Bar, Buffet und allem drumrum.

**Bitte nehmt warme Kleidung mit!** Sofern das Wetter mitspielt werden wir
den Abend *draussen* verbringen. Dort ist zwar alles eingerichtet, um die ersten
Frühlingsabende geniessen zu können, aber Mitte April wird es trotzdem schon auch
mal noch kühl. Einen Plan B haben wir natürlich auch an Lager, falls es nach
zu kühlem oder sonst unpassendem Wetter ausschaut. Entsprechende Entscheidungen
treffen und kommunizieren wir am Durchführungstag. Wir freuen uns!