---
title: "Home"
date: 2022-11-07T11:47:00+01:00
draft: false
bodyClasses: transparent-page-header
---
{{< splash title="TYPO3camp Schweiz" subtitle="18. - 20. April 2024 - Jugendherberge Zürich" image="images/jugi-zh-aufenthaltsraum.jpg" >}}

{{< panel "Willkommen zum TYPO3camp Schweiz" >}}

Mit dem Gedanken, die Marke TYPO3 in der Schweiz weiter zu stärken, möchten wir vom
TYPO3-Entwickler/Integrator über Projektleiter und Entscheidungsträger bis hin zu TYPO3-Interessierten
alle Interessenbereiche aus der Community zusammen bringen. Sei dabei und teile deine Erfahrungen und
dein Wissen rundum TYPO3.

#t3cch

{{< /panel >}}

# News

{{< newsy-block dateline="Donnerstag, 07. März 2024" title="Offizielle TYPO3 Zertifizierung vor Ort möglich" >}}
Es freut uns, dass wir auch dieses Jahr wieder offizielle TYPO3 Zertifizierungen vor Ort anbieten können.
Die Tests finden am **Samstag, 20.04. um 13:30** statt. Bitte melde dich direkt über den [TYPO3 Shop](https://shop.typo3.com/TYPO3-Certification-TYPO3-Camp-Swiss/TY10146)
für eine Zertifizierung an.
{{< /newsy-block >}}

{{< newsy-block dateline="Donnerstag, 22. Februar 2024" title="Camp-Programm, Warmup, Social Event" >}}
**Jetzt chlepfs!** [Das Programmm](/programm) steht, wo wir uns am Donnerstag Abend zum [Warmup](/location#warmup) treffen wissen wir, und
wo die [Social Event](/location#social-event-mönchhof-am-see) Sause stattfindet ist jetzt auch in Stein gemeisselt. 

**[Holt euch jetzt eure Tickets](/tickets)**.
{{< /newsy-block >}}

{{< newsy-block dateline="Sonntag, 11. Februar 2024" title="TYPO3 Association General Assembly vor dem Camp" >}}
Super Neuigkeiten: Die ["General Assembly" (Hauptversammlung) der TYPO3 Association](https://typo3.org/article/typo3-association-general-assembly-18-april-2024-in-zuerich-switzerland)
findet - wie das TYPO3camp Schweiz - in der [Jugendherberge Zürich](/location) statt. Aber es kommt noch dicker: Die General Assembly
findet nur einen Tag vor dem Camp statt (Donnerstag). Wenn das mal nicht eine top Gelegenheit ist, an der GA und dann gleich
noch an einem Barcamp teilzunehmen!

- [Alles zur "General Assembly" der TYPO3 Association](https://typo3.org/article/typo3-association-general-assembly-18-april-2024-in-zuerich-switzerland)
- [Tickets für das TYPO3camp](/tickets)
- [Wo das alles stattfindet](/location)
{{< /newsy-block >}}


{{< newsy-block dateline="Mittwoch, 6. Dezember 2023" title="Camp 2024 in Zürich - ran an die Tickets!" >}}
Nun ist es amtlich: Das TYPO3camp Schweiz 2024 findet in der **Jugendherberge Zürich** statt. Die Jugi hat nicht
nur viel Platz für das Camp und ist perfekt erreichbar, sondern bietet natürlich auch eine prima
Übernachtungsgelegenheit. 🤩 [Alles dazu unter "Location"](/location).

Gleichzeitig geht natürlich auch der Vorverkauf der Tickets los. Jetzt ist die Gelegenheit, sich ein Early-Bird
Ticket zu schnappen. CHF 75.–: Ein echter No-brainer, oder? [Alle Infos zu den Tickets gibt's hier](/tickets).
{{< /newsy-block >}}

---

## Was ist ein TYPO3camp?
Das "TYPO3camp" ist ein zweitägiger Anlass, welcher im Barcamp-Stil durchgeführt wird. Oft werden natürlich
Themen bearbeitet, welche irgendwas mit [TYPO3](//typo3.org) zu tun haben, aber das Camp ist komplett offen, um Themen
aufzunehmen, die unabhängig von TYPO3 auf Interesse stossen. [Wie das genau gehen soll und weiteres
zur Struktur des Camps findest du hier](/format).
